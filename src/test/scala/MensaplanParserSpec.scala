package test.scala

import main.scala.MensaplanParser
import org.scalatest.WordSpec

class MensaplanParserSpec extends WordSpec {
  val dslInstanceExample: String =
    """
      | Menü {
      |   Vorspeise {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   Hauptspeise {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   Nachspeise {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   Tag Montag
      | }
      | Menü {
      |   Vorspeise {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   Hauptspeise {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   Nachspeise {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   Tag Montag
      | }
      | Buffet {
      |   {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      | }
      | Getränkeauswahl {
      |   {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      |   {
      |     Beschreibung ""
      |     Feldinformationen ""
      |     Inhaltsstoffe ""
      |     Gerichtskategorie ""
      |     Preis {
      |       Student : 1.50
      |       Mitarbeiter : 2.50
      |     }
      |   }
      | }
      |""".stripMargin

  "Parser" should {
    val parser: MensaplanParser = new MensaplanParser()

    "parse DSL String" in {
     parser.parseDSL(dslInstanceExample) match {
       case Right(mensaplan) => {
         // parsing was successfull continue with testing the generator
         "generate html" in {

           //val htmlContent = twirl.mens//mensaplangenerator(mensaplan)
         }
       }
       case Left(errorMessage) => {
         fail("Parsing Failed!!\n" + errorMessage)
       }
     }

    }
  }
}


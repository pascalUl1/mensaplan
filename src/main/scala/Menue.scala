package main.scala

case class Menue(
                  vorspeise: Mahlzeit,
                  hauptspeise: Mahlzeit,
                  nachspeise: Mahlzeit,
                  tag: String
                )

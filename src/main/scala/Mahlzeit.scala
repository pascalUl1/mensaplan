package main.scala

case class Mahlzeit(
                     beschreibung: String,
                     feldinformationen: String,
                     inhaltsstoffe: String,
                     gerichtskategorie: String,
                     preise: List[Preis]
                   )

package main.scala

import scala.util.parsing.combinator.RegexParsers

class MensaplanParser extends RegexParsers {

  /**
   * Parse a Mensaplan DSL instance(string)
   *
   * @param input Mensaplan DSL instance
   * @return A Mensaplan model object on success(right) otherwise a failure(left) message
   */
  def parseDSL(input: String): Either[String, Mensaplan] =
    // TODO: Think about adding a Transformer for additional validation steps on the parsed model
    parseAll(mensaplanParser, input) match {
      case Success(t, _) => Right(t)
      case NoSuccess(msg, next) =>
        val pos = next.pos
        Left(s"[$pos] failed parsing: $msg \n\n${pos.longString})")
    }

  private def mensaplanParser: Parser[Mensaplan] =
    (rep1(menue) <~ "Buffet {") ~
      (rep1("{" ~> mahlzeit <~ "}") <~ "}" ~ "Getränkeauswahl {") ~
      rep1("{" ~> mahlzeit <~! "}") <~ "}" ^^ {
      case m ~ b ~ g =>
        Mensaplan(m, b, g)
    }

  private def menue: Parser[Menue] =
    "Menü {" ~
      "Vorspeise {" ~ mahlzeit ~ "}" ~
      "Hauptspeise {" ~ mahlzeit ~ "}" ~
      "Nachspeise {" ~ mahlzeit ~ "}" ~
      "Tag" ~ tag ~
      "}" ^^ {
      case _ ~ _ ~ v ~ _ ~ _ ~ h ~ _ ~ _ ~ n ~ _ ~ _ ~ t ~ _ =>
        Menue(v, h, n, t)
    }

  private def mahlzeit: Parser[Mahlzeit] =
    "Beschreibung" ~ beschreibung ~
      "Feldinformationen" ~ feldinformationen ~
      "Inhaltsstoffe" ~ inhaltsstoffe ~
      "Gerichtskategorie" ~ gerichtskategorie ~
      "Preis {" ~ preise ~ "}" ^^ {
      case _ ~ b ~ _ ~ f ~ _ ~ i ~ _ ~ g ~ _ ~ p ~ _ =>
        Mahlzeit(b, f, i, g, p)
    }

  private def beschreibung: Parser[String] = lineOfText // TODO: match multiline text!?

  private def feldinformationen: Parser[String] = lineOfText // TODO: match multiline text!?

  private def inhaltsstoffe: Parser[String] = lineOfText // TODO: match a list of words and store in separate model object!?

  private def gerichtskategorie: Parser[String] = lineOfText // TODO: change this to single word match!?

  private def preise: Parser[List[Preis]] = rep1(preis)

  private def preis: Parser[Preis] =
    (key <~ ":") ~ value ^^ {
      case k ~ v =>
        Preis(k, v)
    }

  // matches a weekday in german
  private def tag: Parser[String] =
    """\b(Montag)\b|\b(Dienstag)\b|\b(Mittwoch)\b|\b(Donnerstag)\b|\b(Freitag)\b""".r

  // matches a line of characters(any character until line break)/ text without line breaks
  private def lineOfText: Parser[String] = """[^\v]+""".r ^^ (_.trim)

  // matches a word with alphabetical characters
  private def key: Parser[String] = """[A-Za-z]+""".r

  // Matches decimal digits with a . as decimal places separator e.g. 1.50, 2.43
  private def value: Parser[Double] = """\d+(\.\d*)?""".r ^^ (_.toDouble)

  // TODO: Remove these ?!
  /*private def kundentyp: Parser[String] =
    list of strings
    Kunden werden am Anfang definiert und müssten in den Preisen als keys validiert werden
   */
}
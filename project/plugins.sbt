// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.2")
// The Twirl plugin
addSbtPlugin("com.typesafe.sbt" % "sbt-twirl" % "1.5.0")

lazy val root = (project in file("."))
  .enablePlugins(SbtTwirl)
  .enablePlugins(PlayScala)
  .settings(
    name := """Mensaplan DSL""",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.1",

    libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2",
    libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )
  )
